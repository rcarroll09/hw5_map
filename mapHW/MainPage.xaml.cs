﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace mapHW
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.082734, -117.275069), Distance.FromMiles(1)));

            var position1 = new Position(33.067854, -117.242685);
            var position2 = new Position(33.118623, -117.320862);
            var position3 = new Position(33.075064, -117.238836);
            var pin1 = new Pin { Type = PinType.Place, Position = position1, Label = "Home", Address="This is where i live" };
            var pin2 = new Pin { Type = PinType.Place, Position = position2, Label = "Vital Climbing Gym", Address="This is where i work out"};
            var pin3 = new Pin { Type = PinType.Place, Position = position3, Label = "Sprouts Farmers Market", Address="This is where i work" };
            MyMap.Pins.Add(pin1);
            MyMap.Pins.Add(pin2);
            MyMap.Pins.Add(pin3);

            var pinlist = new List<string>();
            pinlist.Add("Gym");
            pinlist.Add("Home");
            pinlist.Add("Work");
            PinPicker.ItemsSource = pinlist;

            var mapList = new List<string>();
            mapList.Add("Street");
            mapList.Add("Satellite");
            mapList.Add("Hybrid");
            MapPicker.ItemsSource = mapList;


        }
        public void pinChanged(object sender, EventArgs e)
        {
            if (PinPicker.SelectedIndex == 0) { MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.118623, -117.320862), Distance.FromMiles(1))); }
            if (PinPicker.SelectedIndex == 1) { MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.067854, -117.242685), Distance.FromMiles(1))); }
            if (PinPicker.SelectedIndex == 2) { MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.075064, -117.238836), Distance.FromMiles(1))); }
        }
        public void MapChanged(object sender, EventArgs e)
        {
            if (MapPicker.SelectedIndex == 0) { MyMap.MapType = MapType.Street; };
            if (MapPicker.SelectedIndex == 1) { MyMap.MapType = MapType.Satellite; };
            if (MapPicker.SelectedIndex == 2) { MyMap.MapType = MapType.Hybrid; };


        }
    }
}
